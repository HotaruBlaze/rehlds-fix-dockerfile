FROM ubuntu:18.04
LABEL maintainer "MrFlutters <flutters@fluttershub.com>"

# Required for Jenkins
WORKDIR /home/jenkins
ARG user=jenkins
ARG group=jenkins
ARG uid=1000
ARG gid=1000
ENV JENKINS_HOME /home/${user}
RUN groupadd -g ${gid} ${group} \
    && useradd -d "$JENKINS_HOME" -u ${uid} -g ${gid} -m -s /bin/bash ${user}
RUN chown -R ${user}:${user} /home/${user}
RUN echo "${user}    ALL=(ALL)    ALL" >> /etc/sudoers


RUN dpkg --add-architecture i386
RUN apt update
RUN apt install -y \
    curl \
    wget \
    file \
    bzip2 \
    gzip \
    bsdmainutils \
    util-linux \
    ca-certificates \
    binutils \
    bc \
    jq \
    lib32gcc1 \
    libstdc++6 \
    libstdc++6:i386 \
    gcc \
    gcc-multilib \
    g++-multilib \
    openjdk-8-jre-headless \
    git
RUN apt-get clean
RUN java -version